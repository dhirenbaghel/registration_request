create index IX_4D69DAE1 on registration_request (groupId, status);
create index IX_C69EDE75 on registration_request (status);
create index IX_FDA03AC3 on registration_request (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_986E9605 on registration_request (uuid_[$COLUMN_LENGTH:75$], groupId);