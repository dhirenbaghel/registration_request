/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package net.ofb.pis.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import net.ofb.pis.model.RegistrationRequest;

/**
 * The cache model class for representing RegistrationRequest in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class RegistrationRequestCacheModel
	implements CacheModel<RegistrationRequest>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RegistrationRequestCacheModel)) {
			return false;
		}

		RegistrationRequestCacheModel registrationRequestCacheModel =
			(RegistrationRequestCacheModel)obj;

		if (registrationRequestId ==
				registrationRequestCacheModel.registrationRequestId) {

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, registrationRequestId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(61);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", registrationRequestId=");
		sb.append(registrationRequestId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append(", unit=");
		sb.append(unit);
		sb.append(", personalNo=");
		sb.append(personalNo);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", genderId=");
		sb.append(genderId);
		sb.append(", designationId=");
		sb.append(designationId);
		sb.append(", tradeId=");
		sb.append(tradeId);
		sb.append(", gradeId=");
		sb.append(gradeId);
		sb.append(", dateOfBirth=");
		sb.append(dateOfBirth);
		sb.append(", dateOfJoiningOrganization=");
		sb.append(dateOfJoiningOrganization);
		sb.append(", casteCategoryId=");
		sb.append(casteCategoryId);
		sb.append(", mobile=");
		sb.append(mobile);
		sb.append(", emailAddress=");
		sb.append(emailAddress);
		sb.append(", dateOfJoiningUnit=");
		sb.append(dateOfJoiningUnit);
		sb.append(", gpf=");
		sb.append(gpf);
		sb.append(", nps=");
		sb.append(nps);
		sb.append(", npsGpfNumber=");
		sb.append(npsGpfNumber);
		sb.append(", section=");
		sb.append(section);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public RegistrationRequest toEntityModel() {
		RegistrationRequestImpl registrationRequestImpl =
			new RegistrationRequestImpl();

		if (uuid == null) {
			registrationRequestImpl.setUuid("");
		}
		else {
			registrationRequestImpl.setUuid(uuid);
		}

		registrationRequestImpl.setRegistrationRequestId(registrationRequestId);
		registrationRequestImpl.setGroupId(groupId);
		registrationRequestImpl.setCompanyId(companyId);
		registrationRequestImpl.setUserId(userId);

		if (userName == null) {
			registrationRequestImpl.setUserName("");
		}
		else {
			registrationRequestImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			registrationRequestImpl.setCreateDate(null);
		}
		else {
			registrationRequestImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			registrationRequestImpl.setModifiedDate(null);
		}
		else {
			registrationRequestImpl.setModifiedDate(new Date(modifiedDate));
		}

		registrationRequestImpl.setStatus(status);
		registrationRequestImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			registrationRequestImpl.setStatusByUserName("");
		}
		else {
			registrationRequestImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			registrationRequestImpl.setStatusDate(null);
		}
		else {
			registrationRequestImpl.setStatusDate(new Date(statusDate));
		}

		if (unit == null) {
			registrationRequestImpl.setUnit("");
		}
		else {
			registrationRequestImpl.setUnit(unit);
		}

		registrationRequestImpl.setPersonalNo(personalNo);

		if (firstName == null) {
			registrationRequestImpl.setFirstName("");
		}
		else {
			registrationRequestImpl.setFirstName(firstName);
		}

		if (lastName == null) {
			registrationRequestImpl.setLastName("");
		}
		else {
			registrationRequestImpl.setLastName(lastName);
		}

		registrationRequestImpl.setGenderId(genderId);
		registrationRequestImpl.setDesignationId(designationId);
		registrationRequestImpl.setTradeId(tradeId);
		registrationRequestImpl.setGradeId(gradeId);

		if (dateOfBirth == Long.MIN_VALUE) {
			registrationRequestImpl.setDateOfBirth(null);
		}
		else {
			registrationRequestImpl.setDateOfBirth(new Date(dateOfBirth));
		}

		if (dateOfJoiningOrganization == Long.MIN_VALUE) {
			registrationRequestImpl.setDateOfJoiningOrganization(null);
		}
		else {
			registrationRequestImpl.setDateOfJoiningOrganization(
				new Date(dateOfJoiningOrganization));
		}

		registrationRequestImpl.setCasteCategoryId(casteCategoryId);
		registrationRequestImpl.setMobile(mobile);

		if (emailAddress == null) {
			registrationRequestImpl.setEmailAddress("");
		}
		else {
			registrationRequestImpl.setEmailAddress(emailAddress);
		}

		if (dateOfJoiningUnit == Long.MIN_VALUE) {
			registrationRequestImpl.setDateOfJoiningUnit(null);
		}
		else {
			registrationRequestImpl.setDateOfJoiningUnit(
				new Date(dateOfJoiningUnit));
		}

		registrationRequestImpl.setGpf(gpf);
		registrationRequestImpl.setNps(nps);
		registrationRequestImpl.setNpsGpfNumber(npsGpfNumber);

		if (section == null) {
			registrationRequestImpl.setSection("");
		}
		else {
			registrationRequestImpl.setSection(section);
		}

		registrationRequestImpl.resetOriginalValues();

		return registrationRequestImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		registrationRequestId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		status = objectInput.readInt();

		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();
		unit = objectInput.readUTF();

		personalNo = objectInput.readLong();
		firstName = objectInput.readUTF();
		lastName = objectInput.readUTF();

		genderId = objectInput.readLong();

		designationId = objectInput.readLong();

		tradeId = objectInput.readLong();

		gradeId = objectInput.readLong();
		dateOfBirth = objectInput.readLong();
		dateOfJoiningOrganization = objectInput.readLong();

		casteCategoryId = objectInput.readLong();

		mobile = objectInput.readLong();
		emailAddress = objectInput.readUTF();
		dateOfJoiningUnit = objectInput.readLong();

		gpf = objectInput.readBoolean();

		nps = objectInput.readBoolean();

		npsGpfNumber = objectInput.readLong();
		section = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(registrationRequestId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeInt(status);

		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);

		if (unit == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(unit);
		}

		objectOutput.writeLong(personalNo);

		if (firstName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(firstName);
		}

		if (lastName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(lastName);
		}

		objectOutput.writeLong(genderId);

		objectOutput.writeLong(designationId);

		objectOutput.writeLong(tradeId);

		objectOutput.writeLong(gradeId);
		objectOutput.writeLong(dateOfBirth);
		objectOutput.writeLong(dateOfJoiningOrganization);

		objectOutput.writeLong(casteCategoryId);

		objectOutput.writeLong(mobile);

		if (emailAddress == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(emailAddress);
		}

		objectOutput.writeLong(dateOfJoiningUnit);

		objectOutput.writeBoolean(gpf);

		objectOutput.writeBoolean(nps);

		objectOutput.writeLong(npsGpfNumber);

		if (section == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section);
		}
	}

	public String uuid;
	public long registrationRequestId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;
	public String unit;
	public long personalNo;
	public String firstName;
	public String lastName;
	public long genderId;
	public long designationId;
	public long tradeId;
	public long gradeId;
	public long dateOfBirth;
	public long dateOfJoiningOrganization;
	public long casteCategoryId;
	public long mobile;
	public String emailAddress;
	public long dateOfJoiningUnit;
	public boolean gpf;
	public boolean nps;
	public long npsGpfNumber;
	public String section;

}