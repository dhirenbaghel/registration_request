/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package net.ofb.pis.service.impl;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetLinkConstants;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.WorkflowInstanceLink;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.WorkflowInstanceLinkLocalServiceUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowHandlerRegistryUtil;
import com.liferay.portal.kernel.workflow.WorkflowInstance;
import com.liferay.portal.kernel.workflow.WorkflowInstanceManagerUtil;
import com.liferay.portal.kernel.workflow.WorkflowLog;
import com.liferay.portal.kernel.workflow.WorkflowLogManagerUtil;
import com.liferay.portal.kernel.workflow.WorkflowTask;
import com.liferay.portal.kernel.workflow.WorkflowTaskManagerUtil;
import com.liferay.portal.kernel.workflow.comparator.WorkflowComparatorFactoryUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import net.ofb.pis.model.RegistrationRequest;
import net.ofb.pis.model.RegistrationRequestDTO;
import net.ofb.pis.service.base.RegistrationRequestLocalServiceBaseImpl;

/**
 * The implementation of the registration request local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>net.ofb.pis.service.RegistrationRequestLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RegistrationRequestLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=net.ofb.pis.model.RegistrationRequest",
	service = AopService.class
)
public class RegistrationRequestLocalServiceImpl
	extends RegistrationRequestLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>net.ofb.pis.service.RegistrationRequestLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>net.ofb.pis.service.RegistrationRequestLocalServiceUtil</code>.
	 */
	
	@Indexable(type = IndexableType.REINDEX)
	public RegistrationRequest addRegistrationRequest(RegistrationRequestDTO registrationRequestDto) throws PortalException {
		
		ServiceContext serviceContext=ServiceContextThreadLocal.getServiceContext();
		long requestId = CounterLocalServiceUtil.increment(RegistrationRequest.class.getName());
		RegistrationRequest registrationRequest = registrationRequestPersistence.create(requestId);
		long userId = serviceContext.getUserId();
		long groupId = serviceContext.getScopeGroupId();
		User user = userLocalService.getUserById(userId);
		
		registrationRequest = convertToObject(registrationRequest, registrationRequestDto, serviceContext);
		
		registrationRequestPersistence.update(registrationRequest);
		
		resourceLocalService.addResources(user.getCompanyId(), groupId, userId, RegistrationRequest.class.getName(), requestId,
				false, true, true);

		AssetEntry assetEntry = assetEntryLocalService.updateEntry(userId, groupId, registrationRequest.getCreateDate(),
				registrationRequest.getModifiedDate(), RegistrationRequest.class.getName(), requestId, registrationRequest.getUuid(), 0,
				serviceContext.getAssetCategoryIds(), serviceContext.getAssetTagNames(), true, true, null, null, null,
				null, ContentTypes.TEXT_HTML, registrationRequest.getFirstName(), null, null, null, null, 0, 0, null);

		assetLinkLocalService.updateLinks(userId, assetEntry.getEntryId(), serviceContext.getAssetLinkEntryIds(),
				AssetLinkConstants.TYPE_RELATED);

		WorkflowHandlerRegistryUtil.startWorkflowInstance(registrationRequest.getCompanyId(), registrationRequest.getGroupId(), registrationRequest.getUserId(),
				RegistrationRequest.class.getName(), registrationRequest.getPrimaryKey(), registrationRequest, serviceContext);
		return registrationRequest;
	}
	
	@Indexable(type = IndexableType.REINDEX)
	public RegistrationRequest updateRegistrationRequest(RegistrationRequestDTO requestDto) throws PortalException,SystemException {
		ServiceContext serviceContext=ServiceContextThreadLocal.getServiceContext();
		RegistrationRequest request = getRegistrationRequest(requestDto.getRegistrationRequestId());
		request = convertToObject(request, requestDto, serviceContext);
		registrationRequestPersistence.update(request);

		resourceLocalService.updateResources(serviceContext.getCompanyId(), serviceContext.getScopeGroupId(),
				RegistrationRequest.class.getName(), request.getRegistrationRequestId(), serviceContext.getModelPermissions());

		AssetEntry assetEntry = assetEntryLocalService.updateEntry(request.getUserId(), request.getGroupId(),
				request.getCreateDate(), request.getModifiedDate(), RegistrationRequest.class.getName(), request.getRegistrationRequestId(),
				request.getUuid(), 0, serviceContext.getAssetCategoryIds(), serviceContext.getAssetTagNames(), true,
				true, request.getCreateDate(), null, null, null, ContentTypes.TEXT_HTML, request.getFirstName(), null,
				null, null, null, 0, 0, serviceContext.getAssetPriority());

		assetLinkLocalService.updateLinks(serviceContext.getUserId(), assetEntry.getEntryId(),
				serviceContext.getAssetLinkEntryIds(), AssetLinkConstants.TYPE_RELATED);
		return request;	
	}
	
	@Indexable(type = IndexableType.DELETE)
	public RegistrationRequest deleteRegistrationRequest(long requestId) throws PortalException {
		RegistrationRequest request = getRegistrationRequest(requestId);
		ServiceContext serviceContext=ServiceContextThreadLocal.getServiceContext();
		request = deleteRegistrationRequest(request);

		resourceLocalService.deleteResource(serviceContext.getCompanyId(), RegistrationRequest.class.getName(),
				ResourceConstants.SCOPE_INDIVIDUAL, requestId);

		AssetEntry assetEntry = assetEntryLocalService.fetchEntry(RegistrationRequest.class.getName(), requestId);

		assetLinkLocalService.deleteLinks(assetEntry.getEntryId());
		assetEntryLocalService.deleteEntry(assetEntry);
		workflowInstanceLinkLocalService.deleteWorkflowInstanceLinks(request.getCompanyId(), request.getGroupId(),
				RegistrationRequest.class.getName(), request.getRegistrationRequestId());
		return request;

	}
	
	public RegistrationRequest updateStatus(long userId, long requestId, int status, ServiceContext serviceContext)
			throws PortalException, SystemException {

		User user = userLocalService.getUser(userId);
		RegistrationRequest request = getRegistrationRequest(requestId);

		request.setStatus(status);
		request.setStatusByUserId(userId);
		request.setStatusByUserName(user.getFullName());
		request.setStatusDate(new Date());
		registrationRequestPersistence.update(request);

		if (status == WorkflowConstants.STATUS_APPROVED) {
			assetEntryLocalService.updateVisible(RegistrationRequest.class.getName(), requestId, true);
		} else {
			assetEntryLocalService.updateVisible(RegistrationRequest.class.getName(), requestId, false);
		}
		return request;
	}
	
	
	public List<RegistrationRequest> getRegistrationRequests(long groupId){
		return registrationRequestPersistence.findByGroupId(groupId);
	}

	public List<RegistrationRequest> getRegistrationRequests(long groupId, int start, int end, OrderByComparator<RegistrationRequest> obc) {
		return registrationRequestPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<RegistrationRequest> getRegistrationRequests(long groupId, int start, int end) {
		return registrationRequestPersistence.findByGroupId(groupId, start, end);
	}

	public List<RegistrationRequest> getRegistrationRequests(long groupId, int status) throws SystemException {
		return registrationRequestPersistence.findByG_S(groupId, WorkflowConstants.STATUS_APPROVED);
	}
	public int getRegistrationRequestsCount(long groupId) {
		return registrationRequestPersistence.countByGroupId(groupId);
	}
	
	
	public void performWorkFlowACtion(long pisId) throws PortalException {
		
		ServiceContext serviceContext=ServiceContextThreadLocal.getServiceContext();
		long userId = serviceContext.getUserId();
		
		RegistrationRequest registration = registrationRequestPersistence.findByPrimaryKey(pisId);
		
		WorkflowInstanceLink wil= WorkflowInstanceLinkLocalServiceUtil.getWorkflowInstanceLink(registration.getCompanyId(), registration.getGroupId(), RegistrationRequest.class.getName(), registration.getRegistrationRequestId());
		 WorkflowInstance workflowInstance = WorkflowInstanceManagerUtil.getWorkflowInstance(registration.getCompanyId(), wil.getWorkflowInstanceId());
		 Map<String, Serializable> workflowContext = workflowInstance.getWorkflowContext();
		 List<Integer> logTypes_assign = new ArrayList<Integer>();
	     logTypes_assign.add(WorkflowLog.TASK_ASSIGN);
	     List<WorkflowLog> workflowLogs_assign = WorkflowLogManagerUtil.getWorkflowLogsByWorkflowInstance(registration.getCompanyId(), wil.getWorkflowInstanceId(), logTypes_assign, QueryUtil.ALL_POS, QueryUtil.ALL_POS, WorkflowComparatorFactoryUtil.getLogCreateDateComparator(true));

	     if(workflowLogs_assign.size() > 0){  
	    	 
	         long taskId = workflowLogs_assign.get(workflowLogs_assign.size()-1).getWorkflowTaskId();
	         WorkflowTask task = WorkflowTaskManagerUtil.getWorkflowTask(registration.getCompanyId(), taskId);
	         List<User> userList =  WorkflowTaskManagerUtil.getAssignableUsers(registration.getCompanyId(), taskId);
 			if(userList.stream().anyMatch(u->u.getUserId() == userId)) {
 				WorkflowTaskManagerUtil.assignWorkflowTaskToUser(registration.getCompanyId(), userId, taskId, userId, "Auto assign", task.getDueDate(), workflowContext);
 			}
 			
 			this.updateStatus(userId, pisId, 0, serviceContext);
	        WorkflowTaskManagerUtil.completeWorkflowTask(registration.getCompanyId(), registration.getUserId(), taskId, "approve", "next task name", workflowContext);
	     } 
	}
	
	/**
	 * For Approve/reject action button @ FE 
	 */
	private boolean isApprover(RegistrationRequest registration) throws PortalException  {
		//TODO duplicate code. need to refactor
		ServiceContext serviceContext=ServiceContextThreadLocal.getServiceContext();
		long userId = serviceContext.getUserId();
		
		WorkflowInstanceLink wil= WorkflowInstanceLinkLocalServiceUtil.getWorkflowInstanceLink(registration.getCompanyId(), registration.getGroupId(), RegistrationRequest.class.getName(), registration.getRegistrationRequestId());
		 List<Integer> logTypes_assign = new ArrayList<Integer>();
	     logTypes_assign.add(WorkflowLog.TASK_ASSIGN);
	     List<WorkflowLog> workflowLogs_assign = WorkflowLogManagerUtil.getWorkflowLogsByWorkflowInstance(registration.getCompanyId(), wil.getWorkflowInstanceId(), logTypes_assign, QueryUtil.ALL_POS, QueryUtil.ALL_POS, WorkflowComparatorFactoryUtil.getLogCreateDateComparator(true));

	     if(workflowLogs_assign.size() > 0){  
	    	 long taskId = workflowLogs_assign.get(workflowLogs_assign.size()-1).getWorkflowTaskId();
	    	 List<User> userList =  WorkflowTaskManagerUtil.getAssignableUsers(registration.getCompanyId(), taskId);
			return userList.stream().anyMatch(user->user.getUserId()==userId);
	     } else 
	    	 return false;
	}

	public List<RegistrationRequestDTO> getAllRegistrations() throws PortalException {
		return entityToDto(registrationRequestPersistence.findAll());
	}
	
	public List<RegistrationRequestDTO> entityToDto(List<RegistrationRequest> requestList) throws PortalException {
		
		if(null == requestList || requestList.size() == 0)
			return null;
		
		List<RegistrationRequestDTO> pisDtList = new ArrayList<RegistrationRequestDTO>();
		
		for(RegistrationRequest request : requestList) {
			RegistrationRequestDTO requestDto = new RegistrationRequestDTO();
			requestDto.setRegistrationRequestId(request.getRegistrationRequestId());
			requestDto.setUnit(request.getUnit());
			requestDto.setPersonalNo(request.getPersonalNo());
			requestDto.setFirstName(request.getFirstName());
			requestDto.setLastName(request.getLastName());
			requestDto.setGenderId(request.getGenderId());
			requestDto.setDesignationName("");
			requestDto.setDesignationId(request.getDesignationId());
			requestDto.setTradeId(request.getTradeId());
			requestDto.setTrade("");
			requestDto.setGradeId(request.getGradeId());
			requestDto.setGrade("");
			requestDto.setDateOfBirth(request.getDateOfBirth());
			requestDto.setDateOfJoiningOrganization(request.getDateOfJoiningOrganization());
			requestDto.setCasteCategoryId(request.getCasteCategoryId());
			requestDto.setCastCategory("");
			requestDto.setMobile(request.getMobile());
			requestDto.setEmailAddress(request.getEmailAddress());
			requestDto.setDateOfJoiningUnit(request.getDateOfJoiningUnit());
			requestDto.setNpsGpfNumber(request.getNpsGpfNumber());
			requestDto.setGpf(request.isGpf());
			requestDto.setNps(request.isNps());
			requestDto.setSection(request.getSection());
			requestDto.setStatus(WorkflowConstants.getStatusLabel(request.getStatus()));
			requestDto.setApprover(isApprover(request));
			pisDtList.add(requestDto);
		}
		return pisDtList;
	}
	
	private RegistrationRequest convertToObject(RegistrationRequest request, RegistrationRequestDTO requestDto, ServiceContext serviceContext) throws PortalException {
		long groupId = serviceContext.getScopeGroupId();
		long userId = serviceContext.getUserId();
		User user = userLocalService.getUserById(userId);
		Date now = new Date();
		
		request.setUuid(serviceContext.getUuid());
		request.setUserId(userId);
		request.setGroupId(groupId);
		request.setCompanyId(user.getCompanyId());
		request.setUserName(user.getFullName());
		request.setCreateDate(serviceContext.getCreateDate(now));
		request.setModifiedDate(serviceContext.getModifiedDate(now));
		
		request.setFirstName(requestDto.getFirstName());
		request.setUnit(requestDto.getUnit());
		request.setPersonalNo(requestDto.getPersonalNo());
		request.setLastName(requestDto.getLastName());
		request.setGenderId(requestDto.getGenderId());
		request.setDesignationId(requestDto.getDesignationId());
		request.setTradeId(requestDto.getTradeId());
		request.setGradeId(requestDto.getGradeId());
		request.setDateOfBirth(requestDto.getDateOfBirth());
		request.setDateOfJoiningOrganization(requestDto.getDateOfJoiningOrganization());
		request.setCasteCategoryId(requestDto.getCasteCategoryId());
		request.setMobile(requestDto.getMobile());
		request.setEmailAddress(requestDto.getEmailAddress());
		request.setDateOfJoiningUnit(requestDto.getDateOfJoiningUnit());
		request.setNpsGpfNumber(requestDto.getNpsGpfNumber());
		request.setNps(requestDto.isNps());
		request.setGpf(requestDto.isGpf());
		request.setSection(requestDto.getSection());
		
		request.setExpandoBridgeAttributes(serviceContext);
		request.setStatus(WorkflowConstants.STATUS_DRAFT);
		request.setStatusByUserId(userId);
		request.setStatusByUserName(user.getFullName());
		request.setStatusDate(serviceContext.getModifiedDate(null));
		
		return request;
	}
	
}