package net.ofb.pis.web.constants;

/**
 * @author adj_2
 */
public class RegistrationRequestWebPortletKeys {

	public static final String REGISTRATIONREQUESTWEB =
		"net_ofb_pis_web_RegistrationRequestWebPortlet";

}