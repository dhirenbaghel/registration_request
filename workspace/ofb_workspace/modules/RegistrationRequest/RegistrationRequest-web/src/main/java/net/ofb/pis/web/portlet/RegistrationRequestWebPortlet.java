package net.ofb.pis.web.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import net.ofb.pis.model.RegistrationRequestDTO;
import net.ofb.pis.web.constants.RegistrationRequestWebPortletKeys;

/**
 * @author adj_2
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=RegistrationRequestWeb",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + RegistrationRequestWebPortletKeys.REGISTRATIONREQUESTWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class RegistrationRequestWebPortlet extends MVCPortlet {

	@Reference
	private RegistrationRequestHandler registrationRequestHandler;
		
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException ,PortletException {
		List<RegistrationRequestDTO> requestDTOs = new ArrayList<RegistrationRequestDTO>();
		try {
			requestDTOs = registrationRequestHandler.getAllRegistrationRequest();
		} catch (PortalException e) {
			// TODO Handle for Exception
			e.printStackTrace();
		}
		renderRequest.setAttribute("requestList", requestDTOs);
		super.doView(renderRequest, renderResponse);
		
	}
	
	
	@ProcessAction(name ="approve_registration")
	public void approveRegistration(ActionRequest request, ActionResponse response) {
		
		try {
			registrationRequestHandler.performWorkFlowAction(ParamUtil.getLong(request, "requestId"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void deleteRequest(ActionRequest request, ActionResponse response) {
		try {
			registrationRequestHandler.deleteRequest(ParamUtil.getLong(request, "requestId"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//TODO use Excelutil
	public void uploadFileAction(ActionRequest request, ActionResponse response) {
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
		        WebKeys.THEME_DISPLAY);
		long fileEntryId = 0l;
		
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		Enumeration<?> paramEnum = uploadRequest.getParameterNames();
		File tempFile = null;

		while (paramEnum.hasMoreElements()) {
			String parameter = (String) paramEnum.nextElement();

			if (parameter.equalsIgnoreCase("pisDocument")) {
				tempFile = uploadRequest.getFile(parameter);
			}
		}
		
		fileEntryId = DocumentMgtmt.fileUploadByApp(tempFile, themeDisplay, request);
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(tempFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			Workbook workbook = WorkbookFactory.create(inputStream);

			if (workbook != null) {
				Sheet sheet = workbook.getSheetAt(0);

				 for (Row row : sheet) {
					 if (row == sheet.getRow(0)) continue;
					 try {
						 RegistrationRequestDTO pisDto = assignUser(row);
						 pisDto.setFileEntryId(fileEntryId);
						 registrationRequestHandler.addRegistrationRequest(assignUser(row));
					} catch (Exception e) {
						e.printStackTrace();
					}
				 }
			}
		} catch (EncryptedDocumentException | IOException e) {
			e.printStackTrace();
		}
	}
	
	// TO be remove
	
	private RegistrationRequestDTO assignUser(Row row) {
		RegistrationRequestDTO pisDto = new RegistrationRequestDTO();
		   pisDto.setUnit(row.getCell(1).getStringCellValue());
		   pisDto.setPersonalNo((long)row.getCell(2).getNumericCellValue());
		   pisDto.setFirstName(row.getCell(3).getStringCellValue());
		   pisDto.setLastName(row.getCell(4).getStringCellValue());
		   pisDto.setGenderName(row.getCell(5).getStringCellValue());
		   pisDto.setDesignationName(row.getCell(6).getStringCellValue());
		   pisDto.setTrade(row.getCell(7).getStringCellValue());
		   pisDto.setGrade(row.getCell(8).getStringCellValue());
		   pisDto.setDateOfBirth(row.getCell(9).getDateCellValue());
		   pisDto.setDateOfJoiningOrganization(row.getCell(10).getDateCellValue());
		   pisDto.setCastCategory(row.getCell(11).getStringCellValue());
		   pisDto.setMobile((long)row.getCell(12).getNumericCellValue());
		   pisDto.setEmailAddress(row.getCell(13).getStringCellValue());
		   pisDto.setDateOfJoiningUnit(row.getCell(14).getDateCellValue());
		   if(row.getCell(15).getStringCellValue().equalsIgnoreCase("GPF"))
			   pisDto.setGpf(true);
		   else
			   pisDto.setNps(true);
		   pisDto.setNpsGpfNumber((long)row.getCell(16).getNumericCellValue());
		   pisDto.setSection(row.getCell(17).getStringCellValue());
		   return pisDto;
	}
	
	
}