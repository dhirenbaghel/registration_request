package net.ofb.pis.web.portlet;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.MimeTypesUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.portlet.ActionRequest;

public class DocumentMgtmt {

	public static long fileUploadByApp(File file,ThemeDisplay themeDisplay,ActionRequest renderRequest)
	{
		 String folderName = "Test";
		//TODO if folder not available then create
	    System.out.println("Exist=>"+file.exists());
		long repositoryId = themeDisplay.getScopeGroupId();
	 	String mimeType = MimeTypesUtil.getContentType(file);
		String title = file.getName();
		String description = "This file is added via programatically";
		String changeLog = "hi";
		Long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
		FileEntry fileEntry = null;
	    try
	    {  
	    	Folder folder =DLAppServiceUtil.getFolder(themeDisplay.getScopeGroupId(), parentFolderId, folderName);
	    	ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(), renderRequest);
	    	InputStream is = new FileInputStream( file );
	    	fileEntry = DLAppServiceUtil.addFileEntry(repositoryId, folder.getFolderId(), file.getName(), mimeType, 
	    			title, description, changeLog, is, file.getTotalSpace(), serviceContext);
	    	
	     } catch (Exception e)
	    {
	       System.out.println("Exception");
	    	e.printStackTrace();
	    }
	    return fileEntry.getFileEntryId();
	    
	}
}
