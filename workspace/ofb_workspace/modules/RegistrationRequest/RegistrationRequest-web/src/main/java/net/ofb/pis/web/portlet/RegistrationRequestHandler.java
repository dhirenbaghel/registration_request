package net.ofb.pis.web.portlet;

import com.liferay.portal.kernel.exception.PortalException;

import java.util.List;

import net.ofb.pis.model.RegistrationRequest;
import net.ofb.pis.model.RegistrationRequestDTO;

public interface RegistrationRequestHandler {

	public List<RegistrationRequestDTO> getAllRegistrationRequest() throws PortalException;
	
	public void performWorkFlowAction(long requestId) throws PortalException;
	
	public RegistrationRequest addRegistrationRequest(RegistrationRequestDTO registrationRequestDTO);

	public RegistrationRequest deleteRequest(long requestId);
}
