package net.ofb.pis.web.portlet;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import net.ofb.pis.model.RegistrationRequest;
import net.ofb.pis.model.RegistrationRequestDTO;
import net.ofb.pis.service.RegistrationRequestLocalService;
import net.ofb.pis.service.RegistrationRequestLocalServiceUtil;

@Component
public class RegistrationRequestHandlerImpl implements RegistrationRequestHandler{
	
	@Reference(unbind = "-")
    protected void setRegistrationRequestService(RegistrationRequestLocalService registrationRequestLocalService) {
		_registrationRequestLocalService = registrationRequestLocalService;
    }

    private RegistrationRequestLocalService _registrationRequestLocalService;


	public List<RegistrationRequestDTO> getAllRegistrationRequest() throws PortalException{
		return _registrationRequestLocalService.getAllRegistrations();
	}
	
	
	
	@Override
	public void performWorkFlowAction(long requestId) throws PortalException {
		_registrationRequestLocalService.performWorkFlowACtion(requestId);
		
	}

	@Override
	public RegistrationRequest addRegistrationRequest(RegistrationRequestDTO registrationRequestDTO) {
		try {
			return _registrationRequestLocalService.addRegistrationRequest(registrationRequestDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public RegistrationRequest deleteRequest(long requestId) {
		try {
			return _registrationRequestLocalService.deleteRegistrationRequest(requestId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
