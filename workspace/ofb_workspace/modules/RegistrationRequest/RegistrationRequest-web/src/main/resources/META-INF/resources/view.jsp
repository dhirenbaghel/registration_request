<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@ include file="/init.jsp" %>
<%@ page import="java.util.List" %>
<%@ page import="com.liferay.portal.kernel.util.ListUtil" %>
<%@page import= "net.ofb.pis.model.RegistrationRequest"%>
<%@page import= "net.ofb.pis.model.RegistrationRequestDTO"%>
 <%@ taglib prefix="aui" uri="http://liferay.com/tld/aui" %> 
 
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 
 

<script src= "https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src= "https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<link rel= "stylesheet" href= "https://cdn.datatables.net/1.10.7/css/jquery.dataTables.css">


 <style>
.message-container{
 padding: 10px;
 margin: 2px;
 display: none;
 background:  rgba(128, 128, 128, 0.33);
 border:  1px solid #0A0A0C;
 }
 
 
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

.modal-content {
  background-color: #fefefe;
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 80%; /* Could be more or less, depending on screen size */
}

.close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}
 
 
 <%-- For Serial Number --%>
 body
{
    counter-reset: Count-Value;     
}
table
{
    border-collapse: separate;
}
tr td:first-child:before
{
  counter-increment: Count-Value;   
  content: counter(Count-Value);
}
 
</style>


 <portlet:renderURL var="editPisURL">
        <portlet:param name="mvcPath" value="/edit_pis.jsp" />
 </portlet:renderURL>
 
 
<portlet:actionURL var="uploadFile" name="uploadFileAction"> </portlet:actionURL>


<div col-md-12>
	<button id="uploadBtn">Upload PIS</button>
	
</div>
<div id="myModal" class="modal">

  <div class="modal-content">
    
    <div>
    	<span class="close" >&times;</span>
    </div>
		<div id="dialog" title="Dialog box" >
		 	<aui:form name="myForm" enctype="multipart/form-data" action="<%=uploadFile %>">
			    <aui:input type="file" name="pisDocument" label="Select File"></aui:input>
			    <aui:button type="submit" name="btnUploadFile" value="Upload File"></aui:button>
			</aui:form>
		</div>
		
  </div>

</div>
<% 
if(request.getAttribute("requestList") != null){
List<RegistrationRequestDTO> requestList = (List<RegistrationRequestDTO>)request.getAttribute("requestList");
%>

<table id= "userTable" class= "display" cellspacing= "0" width= "100%">
        <thead>
            <tr>
            	<th>Sr No</th>
           		<th>Select</th>
                <th>Unit</th>
                <th>Personal No</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email Address</th>
                <th>Mobile</th>
                <th>Workflow Status</th>
                <th>Assign To Me</th>
            </tr>
        </thead>
        
        <tbody>
        	
 <% for(RegistrationRequestDTO requestDto:requestList){ %>
 
	<portlet:actionURL name="deleteRequest" var="deleteURL">
            <portlet:param name="requestId"
                value="<%=String.valueOf(requestDto.getRegistrationRequestId())%>" />
   </portlet:actionURL>
   
   <portlet:actionURL name="approve_registration" var="approveURL">
            <portlet:param name="requestId"
                value="<%=String.valueOf(requestDto.getRegistrationRequestId())%>" /> 
   </portlet:actionURL>
 
   
            <tr>
            	<td></td>
           		 <td><input type="checkbox" name="<portlet:namespace/>row_sel" class="row_sel" value="<%=requestDto.getRegistrationRequestId()%>" /></td>
                 <td><%=requestDto.getRegistrationRequestId()%></td>
                 <td><%=requestDto.getPersonalNo()%></td>
                 <td><%=requestDto.getFirstName()%></td>
                 <td><%=requestDto.getLastName()%></td>
                 <td><%=requestDto.getEmailAddress()%></td>
                 <td><%=requestDto.getMobile()%></td>
                 <td><%=requestDto.getStatus()%></td>
				 <td class="text-center" style="width: 50px"><a
					href="${approveURL}"
					class="btn  btn-primary btn-default btn-sm px-2 py-1">
	                <i class="glyphicon glyphicon-edit">Approve</i>
	                </a>
	              </td>
	              <td class="text-center" style="width: 50px"><a
					href="${deleteURL}"
					class="btn  btn-primary btn-default btn-sm px-2 py-1">
	                <i class="glyphicon glyphicon-edit">Delete</i>
	                </a>
	              </td>
			
            
		</tr>
          <%} %>
         </tbody>
    </table>
<%
} else {
%>
<h1>Nothing to show</h1>
<%
} 
%>

<script>
$(document).ready( function() {
    $('# userTable').DataTable();
} );

// For Modal
var modal = document.getElementById("myModal");
var btn = document.getElementById("uploadBtn");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
  modal.style.display = "block";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>