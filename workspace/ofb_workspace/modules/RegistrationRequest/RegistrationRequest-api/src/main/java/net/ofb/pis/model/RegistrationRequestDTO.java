package net.ofb.pis.model;

import java.util.Date;
import java.util.List;

public class RegistrationRequestDTO {

	private long registrationRequestId;
	private String status;
	private Date statusDate;
	private String unit;
	private long personalNo;
	private String firstName;
	private String lastName;
	private long genderId;
	private String genderName;
	private long designationId;
	private String designationName;
	private long tradeId;
	private String trade;
	private long gradeId;
	private String Grade;
	private Date dateOfBirth;
	private Date dateOfJoiningOrganization;
	private long casteCategoryId;
	private String castCategory;
	private long mobile;
	private String emailAddress;
	private Date dateOfJoiningUnit;
	private boolean isGpf;
	private boolean isNps;
	private long npsGpfNumber;
	private String section;
	private long fileEntryId;
	private boolean isApprover;
	
	private List<String> actions;
	
	public List<String> getActions() {
		return actions;
	}

	public void setActions(List<String> actions) {
		this.actions = actions;
	}

	public long getRegistrationRequestId() {
		return registrationRequestId;
	}

	public void setRegistrationRequestId(long registrationRequestId) {
		this.registrationRequestId = registrationRequestId;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public long getGenderId() {
		return genderId;
	}
	public void setGenderId(long genderId) {
		this.genderId = genderId;
	}
	public long getDesignationId() {
		return designationId;
	}
	public void setDesignationId(long designationId) {
		this.designationId = designationId;
	}
	public long getTradeId() {
		return tradeId;
	}
	public void setTradeId(long tradeId) {
		this.tradeId = tradeId;
	}
	public long getGradeId() {
		return gradeId;
	}
	public void setGradeId(long gradeId) {
		this.gradeId = gradeId;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Date getDateOfJoiningOrganization() {
		return dateOfJoiningOrganization;
	}
	public void setDateOfJoiningOrganization(Date dateOfJoiningOrganization) {
		this.dateOfJoiningOrganization = dateOfJoiningOrganization;
	}
	public long getCasteCategoryId() {
		return casteCategoryId;
	}
	public void setCasteCategoryId(long casteCategoryId) {
		this.casteCategoryId = casteCategoryId;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public Date getDateOfJoiningUnit() {
		return dateOfJoiningUnit;
	}
	public void setDateOfJoiningUnit(Date dateOfJoiningUnit) {
		this.dateOfJoiningUnit = dateOfJoiningUnit;
	}
	
	public long getNpsGpfNumber() {
		return npsGpfNumber;
	}

	public void setNpsGpfNumber(long npsGpfNumber) {
		this.npsGpfNumber = npsGpfNumber;
	}

	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getGenderName() {
		return genderName;
	}
	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getTrade() {
		return trade;
	}
	public void setTrade(String trade) {
		this.trade = trade;
	}
	public String getGrade() {
		return Grade;
	}
	public void setGrade(String grade) {
		Grade = grade;
	}
	public String getCastCategory() {
		return castCategory;
	}
	public void setCastCategory(String castCategory) {
		this.castCategory = castCategory;
	}
	public boolean isGpf() {
		return isGpf;
	}

	public void setGpf(boolean isGpf) {
		this.isGpf = isGpf;
	}

	public boolean isNps() {
		return isNps;
	}

	public void setNps(boolean isNps) {
		this.isNps = isNps;
	}

	public long getFileEntryId() {
		return fileEntryId;
	}

	public void setFileEntryId(long fileEntryId) {
		this.fileEntryId = fileEntryId;
	}

	public boolean isApprover() {
		return isApprover;
	}

	public void setApprover(boolean isApprover) {
		this.isApprover = isApprover;
	}

	public long getPersonalNo() {
		return personalNo;
	}

	public void setPersonalNo(long personalNo) {
		this.personalNo = personalNo;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	
}
