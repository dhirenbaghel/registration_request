/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package net.ofb.pis.service;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for RegistrationRequest. This utility wraps
 * <code>net.ofb.pis.service.impl.RegistrationRequestLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see RegistrationRequestLocalService
 * @generated
 */
public class RegistrationRequestLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>net.ofb.pis.service.impl.RegistrationRequestLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the registration request to the database. Also notifies the appropriate model listeners.
	 *
	 * @param registrationRequest the registration request
	 * @return the registration request that was added
	 */
	public static net.ofb.pis.model.RegistrationRequest addRegistrationRequest(
		net.ofb.pis.model.RegistrationRequest registrationRequest) {

		return getService().addRegistrationRequest(registrationRequest);
	}

	public static net.ofb.pis.model.RegistrationRequest addRegistrationRequest(
			net.ofb.pis.model.RegistrationRequestDTO registrationRequestDto)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().addRegistrationRequest(registrationRequestDto);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			createPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new registration request with the primary key. Does not add the registration request to the database.
	 *
	 * @param registrationRequestId the primary key for the new registration request
	 * @return the new registration request
	 */
	public static net.ofb.pis.model.RegistrationRequest
		createRegistrationRequest(long registrationRequestId) {

		return getService().createRegistrationRequest(registrationRequestId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the registration request with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param registrationRequestId the primary key of the registration request
	 * @return the registration request that was removed
	 * @throws PortalException if a registration request with the primary key could not be found
	 */
	public static net.ofb.pis.model.RegistrationRequest
			deleteRegistrationRequest(long registrationRequestId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteRegistrationRequest(registrationRequestId);
	}

	/**
	 * Deletes the registration request from the database. Also notifies the appropriate model listeners.
	 *
	 * @param registrationRequest the registration request
	 * @return the registration request that was removed
	 */
	public static net.ofb.pis.model.RegistrationRequest
		deleteRegistrationRequest(
			net.ofb.pis.model.RegistrationRequest registrationRequest) {

		return getService().deleteRegistrationRequest(registrationRequest);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>net.ofb.pis.model.impl.RegistrationRequestModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>net.ofb.pis.model.impl.RegistrationRequestModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static java.util.List<net.ofb.pis.model.RegistrationRequestDTO>
			entityToDto(
				java.util.List<net.ofb.pis.model.RegistrationRequest>
					requestList)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().entityToDto(requestList);
	}

	public static net.ofb.pis.model.RegistrationRequest
		fetchRegistrationRequest(long registrationRequestId) {

		return getService().fetchRegistrationRequest(registrationRequestId);
	}

	/**
	 * Returns the registration request matching the UUID and group.
	 *
	 * @param uuid the registration request's UUID
	 * @param groupId the primary key of the group
	 * @return the matching registration request, or <code>null</code> if a matching registration request could not be found
	 */
	public static net.ofb.pis.model.RegistrationRequest
		fetchRegistrationRequestByUuidAndGroupId(String uuid, long groupId) {

		return getService().fetchRegistrationRequestByUuidAndGroupId(
			uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static java.util.List<net.ofb.pis.model.RegistrationRequestDTO>
			getAllRegistrations()
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getAllRegistrations();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the registration request with the primary key.
	 *
	 * @param registrationRequestId the primary key of the registration request
	 * @return the registration request
	 * @throws PortalException if a registration request with the primary key could not be found
	 */
	public static net.ofb.pis.model.RegistrationRequest getRegistrationRequest(
			long registrationRequestId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getRegistrationRequest(registrationRequestId);
	}

	/**
	 * Returns the registration request matching the UUID and group.
	 *
	 * @param uuid the registration request's UUID
	 * @param groupId the primary key of the group
	 * @return the matching registration request
	 * @throws PortalException if a matching registration request could not be found
	 */
	public static net.ofb.pis.model.RegistrationRequest
			getRegistrationRequestByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getRegistrationRequestByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the registration requests.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>net.ofb.pis.model.impl.RegistrationRequestModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of registration requests
	 * @param end the upper bound of the range of registration requests (not inclusive)
	 * @return the range of registration requests
	 */
	public static java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequests(int start, int end) {

		return getService().getRegistrationRequests(start, end);
	}

	public static java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequests(long groupId) {

		return getService().getRegistrationRequests(groupId);
	}

	public static java.util.List<net.ofb.pis.model.RegistrationRequest>
			getRegistrationRequests(long groupId, int status)
		throws com.liferay.portal.kernel.exception.SystemException {

		return getService().getRegistrationRequests(groupId, status);
	}

	public static java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequests(long groupId, int start, int end) {

		return getService().getRegistrationRequests(groupId, start, end);
	}

	public static java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequests(
			long groupId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<net.ofb.pis.model.RegistrationRequest> obc) {

		return getService().getRegistrationRequests(groupId, start, end, obc);
	}

	/**
	 * Returns all the registration requests matching the UUID and company.
	 *
	 * @param uuid the UUID of the registration requests
	 * @param companyId the primary key of the company
	 * @return the matching registration requests, or an empty list if no matches were found
	 */
	public static java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequestsByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getRegistrationRequestsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of registration requests matching the UUID and company.
	 *
	 * @param uuid the UUID of the registration requests
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of registration requests
	 * @param end the upper bound of the range of registration requests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching registration requests, or an empty list if no matches were found
	 */
	public static java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequestsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<net.ofb.pis.model.RegistrationRequest> orderByComparator) {

		return getService().getRegistrationRequestsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of registration requests.
	 *
	 * @return the number of registration requests
	 */
	public static int getRegistrationRequestsCount() {
		return getService().getRegistrationRequestsCount();
	}

	public static int getRegistrationRequestsCount(long groupId) {
		return getService().getRegistrationRequestsCount(groupId);
	}

	public static void performWorkFlowACtion(long pisId)
		throws com.liferay.portal.kernel.exception.PortalException {

		getService().performWorkFlowACtion(pisId);
	}

	/**
	 * Updates the registration request in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param registrationRequest the registration request
	 * @return the registration request that was updated
	 */
	public static net.ofb.pis.model.RegistrationRequest
		updateRegistrationRequest(
			net.ofb.pis.model.RegistrationRequest registrationRequest) {

		return getService().updateRegistrationRequest(registrationRequest);
	}

	public static net.ofb.pis.model.RegistrationRequest
			updateRegistrationRequest(
				net.ofb.pis.model.RegistrationRequestDTO requestDto)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return getService().updateRegistrationRequest(requestDto);
	}

	public static net.ofb.pis.model.RegistrationRequest updateStatus(
			long userId, long requestId, int status,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return getService().updateStatus(
			userId, requestId, status, serviceContext);
	}

	public static RegistrationRequestLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<RegistrationRequestLocalService, RegistrationRequestLocalService>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			RegistrationRequestLocalService.class);

		ServiceTracker
			<RegistrationRequestLocalService, RegistrationRequestLocalService>
				serviceTracker =
					new ServiceTracker
						<RegistrationRequestLocalService,
						 RegistrationRequestLocalService>(
							 bundle.getBundleContext(),
							 RegistrationRequestLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}