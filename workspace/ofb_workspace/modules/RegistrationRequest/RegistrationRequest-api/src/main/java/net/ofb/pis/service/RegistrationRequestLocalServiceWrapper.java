/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package net.ofb.pis.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RegistrationRequestLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see RegistrationRequestLocalService
 * @generated
 */
public class RegistrationRequestLocalServiceWrapper
	implements RegistrationRequestLocalService,
			   ServiceWrapper<RegistrationRequestLocalService> {

	public RegistrationRequestLocalServiceWrapper(
		RegistrationRequestLocalService registrationRequestLocalService) {

		_registrationRequestLocalService = registrationRequestLocalService;
	}

	/**
	 * Adds the registration request to the database. Also notifies the appropriate model listeners.
	 *
	 * @param registrationRequest the registration request
	 * @return the registration request that was added
	 */
	@Override
	public net.ofb.pis.model.RegistrationRequest addRegistrationRequest(
		net.ofb.pis.model.RegistrationRequest registrationRequest) {

		return _registrationRequestLocalService.addRegistrationRequest(
			registrationRequest);
	}

	@Override
	public net.ofb.pis.model.RegistrationRequest addRegistrationRequest(
			net.ofb.pis.model.RegistrationRequestDTO registrationRequestDto)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registrationRequestLocalService.addRegistrationRequest(
			registrationRequestDto);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registrationRequestLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Creates a new registration request with the primary key. Does not add the registration request to the database.
	 *
	 * @param registrationRequestId the primary key for the new registration request
	 * @return the new registration request
	 */
	@Override
	public net.ofb.pis.model.RegistrationRequest createRegistrationRequest(
		long registrationRequestId) {

		return _registrationRequestLocalService.createRegistrationRequest(
			registrationRequestId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registrationRequestLocalService.deletePersistedModel(
			persistedModel);
	}

	/**
	 * Deletes the registration request with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param registrationRequestId the primary key of the registration request
	 * @return the registration request that was removed
	 * @throws PortalException if a registration request with the primary key could not be found
	 */
	@Override
	public net.ofb.pis.model.RegistrationRequest deleteRegistrationRequest(
			long registrationRequestId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registrationRequestLocalService.deleteRegistrationRequest(
			registrationRequestId);
	}

	/**
	 * Deletes the registration request from the database. Also notifies the appropriate model listeners.
	 *
	 * @param registrationRequest the registration request
	 * @return the registration request that was removed
	 */
	@Override
	public net.ofb.pis.model.RegistrationRequest deleteRegistrationRequest(
		net.ofb.pis.model.RegistrationRequest registrationRequest) {

		return _registrationRequestLocalService.deleteRegistrationRequest(
			registrationRequest);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _registrationRequestLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _registrationRequestLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>net.ofb.pis.model.impl.RegistrationRequestModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _registrationRequestLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>net.ofb.pis.model.impl.RegistrationRequestModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _registrationRequestLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _registrationRequestLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _registrationRequestLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public java.util.List<net.ofb.pis.model.RegistrationRequestDTO> entityToDto(
			java.util.List<net.ofb.pis.model.RegistrationRequest> requestList)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registrationRequestLocalService.entityToDto(requestList);
	}

	@Override
	public net.ofb.pis.model.RegistrationRequest fetchRegistrationRequest(
		long registrationRequestId) {

		return _registrationRequestLocalService.fetchRegistrationRequest(
			registrationRequestId);
	}

	/**
	 * Returns the registration request matching the UUID and group.
	 *
	 * @param uuid the registration request's UUID
	 * @param groupId the primary key of the group
	 * @return the matching registration request, or <code>null</code> if a matching registration request could not be found
	 */
	@Override
	public net.ofb.pis.model.RegistrationRequest
		fetchRegistrationRequestByUuidAndGroupId(String uuid, long groupId) {

		return _registrationRequestLocalService.
			fetchRegistrationRequestByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _registrationRequestLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List<net.ofb.pis.model.RegistrationRequestDTO>
			getAllRegistrations()
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registrationRequestLocalService.getAllRegistrations();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _registrationRequestLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _registrationRequestLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _registrationRequestLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registrationRequestLocalService.getPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Returns the registration request with the primary key.
	 *
	 * @param registrationRequestId the primary key of the registration request
	 * @return the registration request
	 * @throws PortalException if a registration request with the primary key could not be found
	 */
	@Override
	public net.ofb.pis.model.RegistrationRequest getRegistrationRequest(
			long registrationRequestId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registrationRequestLocalService.getRegistrationRequest(
			registrationRequestId);
	}

	/**
	 * Returns the registration request matching the UUID and group.
	 *
	 * @param uuid the registration request's UUID
	 * @param groupId the primary key of the group
	 * @return the matching registration request
	 * @throws PortalException if a matching registration request could not be found
	 */
	@Override
	public net.ofb.pis.model.RegistrationRequest
			getRegistrationRequestByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registrationRequestLocalService.
			getRegistrationRequestByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the registration requests.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>net.ofb.pis.model.impl.RegistrationRequestModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of registration requests
	 * @param end the upper bound of the range of registration requests (not inclusive)
	 * @return the range of registration requests
	 */
	@Override
	public java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequests(int start, int end) {

		return _registrationRequestLocalService.getRegistrationRequests(
			start, end);
	}

	@Override
	public java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequests(long groupId) {

		return _registrationRequestLocalService.getRegistrationRequests(
			groupId);
	}

	@Override
	public java.util.List<net.ofb.pis.model.RegistrationRequest>
			getRegistrationRequests(long groupId, int status)
		throws com.liferay.portal.kernel.exception.SystemException {

		return _registrationRequestLocalService.getRegistrationRequests(
			groupId, status);
	}

	@Override
	public java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequests(long groupId, int start, int end) {

		return _registrationRequestLocalService.getRegistrationRequests(
			groupId, start, end);
	}

	@Override
	public java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequests(
			long groupId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<net.ofb.pis.model.RegistrationRequest> obc) {

		return _registrationRequestLocalService.getRegistrationRequests(
			groupId, start, end, obc);
	}

	/**
	 * Returns all the registration requests matching the UUID and company.
	 *
	 * @param uuid the UUID of the registration requests
	 * @param companyId the primary key of the company
	 * @return the matching registration requests, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequestsByUuidAndCompanyId(String uuid, long companyId) {

		return _registrationRequestLocalService.
			getRegistrationRequestsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of registration requests matching the UUID and company.
	 *
	 * @param uuid the UUID of the registration requests
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of registration requests
	 * @param end the upper bound of the range of registration requests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching registration requests, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<net.ofb.pis.model.RegistrationRequest>
		getRegistrationRequestsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<net.ofb.pis.model.RegistrationRequest> orderByComparator) {

		return _registrationRequestLocalService.
			getRegistrationRequestsByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of registration requests.
	 *
	 * @return the number of registration requests
	 */
	@Override
	public int getRegistrationRequestsCount() {
		return _registrationRequestLocalService.getRegistrationRequestsCount();
	}

	@Override
	public int getRegistrationRequestsCount(long groupId) {
		return _registrationRequestLocalService.getRegistrationRequestsCount(
			groupId);
	}

	@Override
	public void performWorkFlowACtion(long pisId)
		throws com.liferay.portal.kernel.exception.PortalException {

		_registrationRequestLocalService.performWorkFlowACtion(pisId);
	}

	/**
	 * Updates the registration request in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param registrationRequest the registration request
	 * @return the registration request that was updated
	 */
	@Override
	public net.ofb.pis.model.RegistrationRequest updateRegistrationRequest(
		net.ofb.pis.model.RegistrationRequest registrationRequest) {

		return _registrationRequestLocalService.updateRegistrationRequest(
			registrationRequest);
	}

	@Override
	public net.ofb.pis.model.RegistrationRequest updateRegistrationRequest(
			net.ofb.pis.model.RegistrationRequestDTO requestDto)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _registrationRequestLocalService.updateRegistrationRequest(
			requestDto);
	}

	@Override
	public net.ofb.pis.model.RegistrationRequest updateStatus(
			long userId, long requestId, int status,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _registrationRequestLocalService.updateStatus(
			userId, requestId, status, serviceContext);
	}

	@Override
	public RegistrationRequestLocalService getWrappedService() {
		return _registrationRequestLocalService;
	}

	@Override
	public void setWrappedService(
		RegistrationRequestLocalService registrationRequestLocalService) {

		_registrationRequestLocalService = registrationRequestLocalService;
	}

	private RegistrationRequestLocalService _registrationRequestLocalService;

}