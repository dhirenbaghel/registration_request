/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package net.ofb.pis.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link net.ofb.pis.service.http.RegistrationRequestServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class RegistrationRequestSoap implements Serializable {

	public static RegistrationRequestSoap toSoapModel(
		RegistrationRequest model) {

		RegistrationRequestSoap soapModel = new RegistrationRequestSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setRegistrationRequestId(model.getRegistrationRequestId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setStatus(model.getStatus());
		soapModel.setStatusByUserId(model.getStatusByUserId());
		soapModel.setStatusByUserName(model.getStatusByUserName());
		soapModel.setStatusDate(model.getStatusDate());
		soapModel.setUnit(model.getUnit());
		soapModel.setPersonalNo(model.getPersonalNo());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setLastName(model.getLastName());
		soapModel.setGenderId(model.getGenderId());
		soapModel.setDesignationId(model.getDesignationId());
		soapModel.setTradeId(model.getTradeId());
		soapModel.setGradeId(model.getGradeId());
		soapModel.setDateOfBirth(model.getDateOfBirth());
		soapModel.setDateOfJoiningOrganization(
			model.getDateOfJoiningOrganization());
		soapModel.setCasteCategoryId(model.getCasteCategoryId());
		soapModel.setMobile(model.getMobile());
		soapModel.setEmailAddress(model.getEmailAddress());
		soapModel.setDateOfJoiningUnit(model.getDateOfJoiningUnit());
		soapModel.setGpf(model.isGpf());
		soapModel.setNps(model.isNps());
		soapModel.setNpsGpfNumber(model.getNpsGpfNumber());
		soapModel.setSection(model.getSection());

		return soapModel;
	}

	public static RegistrationRequestSoap[] toSoapModels(
		RegistrationRequest[] models) {

		RegistrationRequestSoap[] soapModels =
			new RegistrationRequestSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static RegistrationRequestSoap[][] toSoapModels(
		RegistrationRequest[][] models) {

		RegistrationRequestSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new RegistrationRequestSoap[models.length][models[0].length];
		}
		else {
			soapModels = new RegistrationRequestSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static RegistrationRequestSoap[] toSoapModels(
		List<RegistrationRequest> models) {

		List<RegistrationRequestSoap> soapModels =
			new ArrayList<RegistrationRequestSoap>(models.size());

		for (RegistrationRequest model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(
			new RegistrationRequestSoap[soapModels.size()]);
	}

	public RegistrationRequestSoap() {
	}

	public long getPrimaryKey() {
		return _registrationRequestId;
	}

	public void setPrimaryKey(long pk) {
		setRegistrationRequestId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getRegistrationRequestId() {
		return _registrationRequestId;
	}

	public void setRegistrationRequestId(long registrationRequestId) {
		_registrationRequestId = registrationRequestId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public long getStatusByUserId() {
		return _statusByUserId;
	}

	public void setStatusByUserId(long statusByUserId) {
		_statusByUserId = statusByUserId;
	}

	public String getStatusByUserName() {
		return _statusByUserName;
	}

	public void setStatusByUserName(String statusByUserName) {
		_statusByUserName = statusByUserName;
	}

	public Date getStatusDate() {
		return _statusDate;
	}

	public void setStatusDate(Date statusDate) {
		_statusDate = statusDate;
	}

	public String getUnit() {
		return _unit;
	}

	public void setUnit(String unit) {
		_unit = unit;
	}

	public long getPersonalNo() {
		return _personalNo;
	}

	public void setPersonalNo(long personalNo) {
		_personalNo = personalNo;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	public long getGenderId() {
		return _genderId;
	}

	public void setGenderId(long genderId) {
		_genderId = genderId;
	}

	public long getDesignationId() {
		return _designationId;
	}

	public void setDesignationId(long designationId) {
		_designationId = designationId;
	}

	public long getTradeId() {
		return _tradeId;
	}

	public void setTradeId(long tradeId) {
		_tradeId = tradeId;
	}

	public long getGradeId() {
		return _gradeId;
	}

	public void setGradeId(long gradeId) {
		_gradeId = gradeId;
	}

	public Date getDateOfBirth() {
		return _dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		_dateOfBirth = dateOfBirth;
	}

	public Date getDateOfJoiningOrganization() {
		return _dateOfJoiningOrganization;
	}

	public void setDateOfJoiningOrganization(Date dateOfJoiningOrganization) {
		_dateOfJoiningOrganization = dateOfJoiningOrganization;
	}

	public long getCasteCategoryId() {
		return _casteCategoryId;
	}

	public void setCasteCategoryId(long casteCategoryId) {
		_casteCategoryId = casteCategoryId;
	}

	public long getMobile() {
		return _mobile;
	}

	public void setMobile(long mobile) {
		_mobile = mobile;
	}

	public String getEmailAddress() {
		return _emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		_emailAddress = emailAddress;
	}

	public Date getDateOfJoiningUnit() {
		return _dateOfJoiningUnit;
	}

	public void setDateOfJoiningUnit(Date dateOfJoiningUnit) {
		_dateOfJoiningUnit = dateOfJoiningUnit;
	}

	public boolean getGpf() {
		return _gpf;
	}

	public boolean isGpf() {
		return _gpf;
	}

	public void setGpf(boolean gpf) {
		_gpf = gpf;
	}

	public boolean getNps() {
		return _nps;
	}

	public boolean isNps() {
		return _nps;
	}

	public void setNps(boolean nps) {
		_nps = nps;
	}

	public long getNpsGpfNumber() {
		return _npsGpfNumber;
	}

	public void setNpsGpfNumber(long npsGpfNumber) {
		_npsGpfNumber = npsGpfNumber;
	}

	public String getSection() {
		return _section;
	}

	public void setSection(String section) {
		_section = section;
	}

	private String _uuid;
	private long _registrationRequestId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private int _status;
	private long _statusByUserId;
	private String _statusByUserName;
	private Date _statusDate;
	private String _unit;
	private long _personalNo;
	private String _firstName;
	private String _lastName;
	private long _genderId;
	private long _designationId;
	private long _tradeId;
	private long _gradeId;
	private Date _dateOfBirth;
	private Date _dateOfJoiningOrganization;
	private long _casteCategoryId;
	private long _mobile;
	private String _emailAddress;
	private Date _dateOfJoiningUnit;
	private boolean _gpf;
	private boolean _nps;
	private long _npsGpfNumber;
	private String _section;

}