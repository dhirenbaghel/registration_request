/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package net.ofb.pis.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link RegistrationRequest}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RegistrationRequest
 * @generated
 */
public class RegistrationRequestWrapper
	extends BaseModelWrapper<RegistrationRequest>
	implements ModelWrapper<RegistrationRequest>, RegistrationRequest {

	public RegistrationRequestWrapper(RegistrationRequest registrationRequest) {
		super(registrationRequest);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("registrationRequestId", getRegistrationRequestId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());
		attributes.put("unit", getUnit());
		attributes.put("personalNo", getPersonalNo());
		attributes.put("firstName", getFirstName());
		attributes.put("lastName", getLastName());
		attributes.put("genderId", getGenderId());
		attributes.put("designationId", getDesignationId());
		attributes.put("tradeId", getTradeId());
		attributes.put("gradeId", getGradeId());
		attributes.put("dateOfBirth", getDateOfBirth());
		attributes.put(
			"dateOfJoiningOrganization", getDateOfJoiningOrganization());
		attributes.put("casteCategoryId", getCasteCategoryId());
		attributes.put("mobile", getMobile());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("dateOfJoiningUnit", getDateOfJoiningUnit());
		attributes.put("gpf", isGpf());
		attributes.put("nps", isNps());
		attributes.put("npsGpfNumber", getNpsGpfNumber());
		attributes.put("section", getSection());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long registrationRequestId = (Long)attributes.get(
			"registrationRequestId");

		if (registrationRequestId != null) {
			setRegistrationRequestId(registrationRequestId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}

		String unit = (String)attributes.get("unit");

		if (unit != null) {
			setUnit(unit);
		}

		Long personalNo = (Long)attributes.get("personalNo");

		if (personalNo != null) {
			setPersonalNo(personalNo);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		Long genderId = (Long)attributes.get("genderId");

		if (genderId != null) {
			setGenderId(genderId);
		}

		Long designationId = (Long)attributes.get("designationId");

		if (designationId != null) {
			setDesignationId(designationId);
		}

		Long tradeId = (Long)attributes.get("tradeId");

		if (tradeId != null) {
			setTradeId(tradeId);
		}

		Long gradeId = (Long)attributes.get("gradeId");

		if (gradeId != null) {
			setGradeId(gradeId);
		}

		Date dateOfBirth = (Date)attributes.get("dateOfBirth");

		if (dateOfBirth != null) {
			setDateOfBirth(dateOfBirth);
		}

		Date dateOfJoiningOrganization = (Date)attributes.get(
			"dateOfJoiningOrganization");

		if (dateOfJoiningOrganization != null) {
			setDateOfJoiningOrganization(dateOfJoiningOrganization);
		}

		Long casteCategoryId = (Long)attributes.get("casteCategoryId");

		if (casteCategoryId != null) {
			setCasteCategoryId(casteCategoryId);
		}

		Long mobile = (Long)attributes.get("mobile");

		if (mobile != null) {
			setMobile(mobile);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		Date dateOfJoiningUnit = (Date)attributes.get("dateOfJoiningUnit");

		if (dateOfJoiningUnit != null) {
			setDateOfJoiningUnit(dateOfJoiningUnit);
		}

		Boolean gpf = (Boolean)attributes.get("gpf");

		if (gpf != null) {
			setGpf(gpf);
		}

		Boolean nps = (Boolean)attributes.get("nps");

		if (nps != null) {
			setNps(nps);
		}

		Long npsGpfNumber = (Long)attributes.get("npsGpfNumber");

		if (npsGpfNumber != null) {
			setNpsGpfNumber(npsGpfNumber);
		}

		String section = (String)attributes.get("section");

		if (section != null) {
			setSection(section);
		}
	}

	/**
	 * Returns the caste category ID of this registration request.
	 *
	 * @return the caste category ID of this registration request
	 */
	@Override
	public long getCasteCategoryId() {
		return model.getCasteCategoryId();
	}

	/**
	 * Returns the company ID of this registration request.
	 *
	 * @return the company ID of this registration request
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this registration request.
	 *
	 * @return the create date of this registration request
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the date of birth of this registration request.
	 *
	 * @return the date of birth of this registration request
	 */
	@Override
	public Date getDateOfBirth() {
		return model.getDateOfBirth();
	}

	/**
	 * Returns the date of joining organization of this registration request.
	 *
	 * @return the date of joining organization of this registration request
	 */
	@Override
	public Date getDateOfJoiningOrganization() {
		return model.getDateOfJoiningOrganization();
	}

	/**
	 * Returns the date of joining unit of this registration request.
	 *
	 * @return the date of joining unit of this registration request
	 */
	@Override
	public Date getDateOfJoiningUnit() {
		return model.getDateOfJoiningUnit();
	}

	/**
	 * Returns the designation ID of this registration request.
	 *
	 * @return the designation ID of this registration request
	 */
	@Override
	public long getDesignationId() {
		return model.getDesignationId();
	}

	/**
	 * Returns the email address of this registration request.
	 *
	 * @return the email address of this registration request
	 */
	@Override
	public String getEmailAddress() {
		return model.getEmailAddress();
	}

	/**
	 * Returns the first name of this registration request.
	 *
	 * @return the first name of this registration request
	 */
	@Override
	public String getFirstName() {
		return model.getFirstName();
	}

	/**
	 * Returns the gender ID of this registration request.
	 *
	 * @return the gender ID of this registration request
	 */
	@Override
	public long getGenderId() {
		return model.getGenderId();
	}

	/**
	 * Returns the gpf of this registration request.
	 *
	 * @return the gpf of this registration request
	 */
	@Override
	public boolean getGpf() {
		return model.getGpf();
	}

	/**
	 * Returns the grade ID of this registration request.
	 *
	 * @return the grade ID of this registration request
	 */
	@Override
	public long getGradeId() {
		return model.getGradeId();
	}

	/**
	 * Returns the group ID of this registration request.
	 *
	 * @return the group ID of this registration request
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the last name of this registration request.
	 *
	 * @return the last name of this registration request
	 */
	@Override
	public String getLastName() {
		return model.getLastName();
	}

	/**
	 * Returns the mobile of this registration request.
	 *
	 * @return the mobile of this registration request
	 */
	@Override
	public long getMobile() {
		return model.getMobile();
	}

	/**
	 * Returns the modified date of this registration request.
	 *
	 * @return the modified date of this registration request
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the nps of this registration request.
	 *
	 * @return the nps of this registration request
	 */
	@Override
	public boolean getNps() {
		return model.getNps();
	}

	/**
	 * Returns the nps gpf number of this registration request.
	 *
	 * @return the nps gpf number of this registration request
	 */
	@Override
	public long getNpsGpfNumber() {
		return model.getNpsGpfNumber();
	}

	/**
	 * Returns the personal no of this registration request.
	 *
	 * @return the personal no of this registration request
	 */
	@Override
	public long getPersonalNo() {
		return model.getPersonalNo();
	}

	/**
	 * Returns the primary key of this registration request.
	 *
	 * @return the primary key of this registration request
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the registration request ID of this registration request.
	 *
	 * @return the registration request ID of this registration request
	 */
	@Override
	public long getRegistrationRequestId() {
		return model.getRegistrationRequestId();
	}

	/**
	 * Returns the section of this registration request.
	 *
	 * @return the section of this registration request
	 */
	@Override
	public String getSection() {
		return model.getSection();
	}

	/**
	 * Returns the status of this registration request.
	 *
	 * @return the status of this registration request
	 */
	@Override
	public int getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the status by user ID of this registration request.
	 *
	 * @return the status by user ID of this registration request
	 */
	@Override
	public long getStatusByUserId() {
		return model.getStatusByUserId();
	}

	/**
	 * Returns the status by user name of this registration request.
	 *
	 * @return the status by user name of this registration request
	 */
	@Override
	public String getStatusByUserName() {
		return model.getStatusByUserName();
	}

	/**
	 * Returns the status by user uuid of this registration request.
	 *
	 * @return the status by user uuid of this registration request
	 */
	@Override
	public String getStatusByUserUuid() {
		return model.getStatusByUserUuid();
	}

	/**
	 * Returns the status date of this registration request.
	 *
	 * @return the status date of this registration request
	 */
	@Override
	public Date getStatusDate() {
		return model.getStatusDate();
	}

	/**
	 * Returns the trade ID of this registration request.
	 *
	 * @return the trade ID of this registration request
	 */
	@Override
	public long getTradeId() {
		return model.getTradeId();
	}

	/**
	 * Returns the unit of this registration request.
	 *
	 * @return the unit of this registration request
	 */
	@Override
	public String getUnit() {
		return model.getUnit();
	}

	/**
	 * Returns the user ID of this registration request.
	 *
	 * @return the user ID of this registration request
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this registration request.
	 *
	 * @return the user name of this registration request
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this registration request.
	 *
	 * @return the user uuid of this registration request
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this registration request.
	 *
	 * @return the uuid of this registration request
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this registration request is approved.
	 *
	 * @return <code>true</code> if this registration request is approved; <code>false</code> otherwise
	 */
	@Override
	public boolean isApproved() {
		return model.isApproved();
	}

	/**
	 * Returns <code>true</code> if this registration request is denied.
	 *
	 * @return <code>true</code> if this registration request is denied; <code>false</code> otherwise
	 */
	@Override
	public boolean isDenied() {
		return model.isDenied();
	}

	/**
	 * Returns <code>true</code> if this registration request is a draft.
	 *
	 * @return <code>true</code> if this registration request is a draft; <code>false</code> otherwise
	 */
	@Override
	public boolean isDraft() {
		return model.isDraft();
	}

	/**
	 * Returns <code>true</code> if this registration request is expired.
	 *
	 * @return <code>true</code> if this registration request is expired; <code>false</code> otherwise
	 */
	@Override
	public boolean isExpired() {
		return model.isExpired();
	}

	/**
	 * Returns <code>true</code> if this registration request is gpf.
	 *
	 * @return <code>true</code> if this registration request is gpf; <code>false</code> otherwise
	 */
	@Override
	public boolean isGpf() {
		return model.isGpf();
	}

	/**
	 * Returns <code>true</code> if this registration request is inactive.
	 *
	 * @return <code>true</code> if this registration request is inactive; <code>false</code> otherwise
	 */
	@Override
	public boolean isInactive() {
		return model.isInactive();
	}

	/**
	 * Returns <code>true</code> if this registration request is incomplete.
	 *
	 * @return <code>true</code> if this registration request is incomplete; <code>false</code> otherwise
	 */
	@Override
	public boolean isIncomplete() {
		return model.isIncomplete();
	}

	/**
	 * Returns <code>true</code> if this registration request is nps.
	 *
	 * @return <code>true</code> if this registration request is nps; <code>false</code> otherwise
	 */
	@Override
	public boolean isNps() {
		return model.isNps();
	}

	/**
	 * Returns <code>true</code> if this registration request is pending.
	 *
	 * @return <code>true</code> if this registration request is pending; <code>false</code> otherwise
	 */
	@Override
	public boolean isPending() {
		return model.isPending();
	}

	/**
	 * Returns <code>true</code> if this registration request is scheduled.
	 *
	 * @return <code>true</code> if this registration request is scheduled; <code>false</code> otherwise
	 */
	@Override
	public boolean isScheduled() {
		return model.isScheduled();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the caste category ID of this registration request.
	 *
	 * @param casteCategoryId the caste category ID of this registration request
	 */
	@Override
	public void setCasteCategoryId(long casteCategoryId) {
		model.setCasteCategoryId(casteCategoryId);
	}

	/**
	 * Sets the company ID of this registration request.
	 *
	 * @param companyId the company ID of this registration request
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this registration request.
	 *
	 * @param createDate the create date of this registration request
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the date of birth of this registration request.
	 *
	 * @param dateOfBirth the date of birth of this registration request
	 */
	@Override
	public void setDateOfBirth(Date dateOfBirth) {
		model.setDateOfBirth(dateOfBirth);
	}

	/**
	 * Sets the date of joining organization of this registration request.
	 *
	 * @param dateOfJoiningOrganization the date of joining organization of this registration request
	 */
	@Override
	public void setDateOfJoiningOrganization(Date dateOfJoiningOrganization) {
		model.setDateOfJoiningOrganization(dateOfJoiningOrganization);
	}

	/**
	 * Sets the date of joining unit of this registration request.
	 *
	 * @param dateOfJoiningUnit the date of joining unit of this registration request
	 */
	@Override
	public void setDateOfJoiningUnit(Date dateOfJoiningUnit) {
		model.setDateOfJoiningUnit(dateOfJoiningUnit);
	}

	/**
	 * Sets the designation ID of this registration request.
	 *
	 * @param designationId the designation ID of this registration request
	 */
	@Override
	public void setDesignationId(long designationId) {
		model.setDesignationId(designationId);
	}

	/**
	 * Sets the email address of this registration request.
	 *
	 * @param emailAddress the email address of this registration request
	 */
	@Override
	public void setEmailAddress(String emailAddress) {
		model.setEmailAddress(emailAddress);
	}

	/**
	 * Sets the first name of this registration request.
	 *
	 * @param firstName the first name of this registration request
	 */
	@Override
	public void setFirstName(String firstName) {
		model.setFirstName(firstName);
	}

	/**
	 * Sets the gender ID of this registration request.
	 *
	 * @param genderId the gender ID of this registration request
	 */
	@Override
	public void setGenderId(long genderId) {
		model.setGenderId(genderId);
	}

	/**
	 * Sets whether this registration request is gpf.
	 *
	 * @param gpf the gpf of this registration request
	 */
	@Override
	public void setGpf(boolean gpf) {
		model.setGpf(gpf);
	}

	/**
	 * Sets the grade ID of this registration request.
	 *
	 * @param gradeId the grade ID of this registration request
	 */
	@Override
	public void setGradeId(long gradeId) {
		model.setGradeId(gradeId);
	}

	/**
	 * Sets the group ID of this registration request.
	 *
	 * @param groupId the group ID of this registration request
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the last name of this registration request.
	 *
	 * @param lastName the last name of this registration request
	 */
	@Override
	public void setLastName(String lastName) {
		model.setLastName(lastName);
	}

	/**
	 * Sets the mobile of this registration request.
	 *
	 * @param mobile the mobile of this registration request
	 */
	@Override
	public void setMobile(long mobile) {
		model.setMobile(mobile);
	}

	/**
	 * Sets the modified date of this registration request.
	 *
	 * @param modifiedDate the modified date of this registration request
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets whether this registration request is nps.
	 *
	 * @param nps the nps of this registration request
	 */
	@Override
	public void setNps(boolean nps) {
		model.setNps(nps);
	}

	/**
	 * Sets the nps gpf number of this registration request.
	 *
	 * @param npsGpfNumber the nps gpf number of this registration request
	 */
	@Override
	public void setNpsGpfNumber(long npsGpfNumber) {
		model.setNpsGpfNumber(npsGpfNumber);
	}

	/**
	 * Sets the personal no of this registration request.
	 *
	 * @param personalNo the personal no of this registration request
	 */
	@Override
	public void setPersonalNo(long personalNo) {
		model.setPersonalNo(personalNo);
	}

	/**
	 * Sets the primary key of this registration request.
	 *
	 * @param primaryKey the primary key of this registration request
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the registration request ID of this registration request.
	 *
	 * @param registrationRequestId the registration request ID of this registration request
	 */
	@Override
	public void setRegistrationRequestId(long registrationRequestId) {
		model.setRegistrationRequestId(registrationRequestId);
	}

	/**
	 * Sets the section of this registration request.
	 *
	 * @param section the section of this registration request
	 */
	@Override
	public void setSection(String section) {
		model.setSection(section);
	}

	/**
	 * Sets the status of this registration request.
	 *
	 * @param status the status of this registration request
	 */
	@Override
	public void setStatus(int status) {
		model.setStatus(status);
	}

	/**
	 * Sets the status by user ID of this registration request.
	 *
	 * @param statusByUserId the status by user ID of this registration request
	 */
	@Override
	public void setStatusByUserId(long statusByUserId) {
		model.setStatusByUserId(statusByUserId);
	}

	/**
	 * Sets the status by user name of this registration request.
	 *
	 * @param statusByUserName the status by user name of this registration request
	 */
	@Override
	public void setStatusByUserName(String statusByUserName) {
		model.setStatusByUserName(statusByUserName);
	}

	/**
	 * Sets the status by user uuid of this registration request.
	 *
	 * @param statusByUserUuid the status by user uuid of this registration request
	 */
	@Override
	public void setStatusByUserUuid(String statusByUserUuid) {
		model.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	 * Sets the status date of this registration request.
	 *
	 * @param statusDate the status date of this registration request
	 */
	@Override
	public void setStatusDate(Date statusDate) {
		model.setStatusDate(statusDate);
	}

	/**
	 * Sets the trade ID of this registration request.
	 *
	 * @param tradeId the trade ID of this registration request
	 */
	@Override
	public void setTradeId(long tradeId) {
		model.setTradeId(tradeId);
	}

	/**
	 * Sets the unit of this registration request.
	 *
	 * @param unit the unit of this registration request
	 */
	@Override
	public void setUnit(String unit) {
		model.setUnit(unit);
	}

	/**
	 * Sets the user ID of this registration request.
	 *
	 * @param userId the user ID of this registration request
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this registration request.
	 *
	 * @param userName the user name of this registration request
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this registration request.
	 *
	 * @param userUuid the user uuid of this registration request
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this registration request.
	 *
	 * @param uuid the uuid of this registration request
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected RegistrationRequestWrapper wrap(
		RegistrationRequest registrationRequest) {

		return new RegistrationRequestWrapper(registrationRequest);
	}

}